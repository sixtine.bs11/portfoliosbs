import './css/style.scss';
import './css/aboutMe.scss';
import './css/navbar.scss';
import './css/comp.scss';
import './css/projects.scss';
import './css/contact.scss';

let navAboutMeDom = document.querySelector('.navAboutMe'),
    navCompDom = document.querySelector('.navComp'),
    navProjectsDom = document.querySelector('.navProjects'),
    navContactDom = document.querySelector('.navContact'),
    compDom = document.querySelector('.comp'),
    aboutMeDom = document.querySelector('.aboutMe'),
    projects = document.querySelector('.projects'),
    contact = document.querySelector('.contact');


// Show or hide depending where I click in the navbar

function hideNav () {
    ulDom.classList.toggle('active');
}

function aboutMeShow() {
    aboutMeDom.style.display = 'block';
    compDom.style.display = 'none';
    projects.style.display = 'none';
    contact.style.display = 'none';
}

navAboutMeDom.addEventListener("click", function () {
    aboutMeShow();
    hideNav();
});

function compShow() {
    aboutMeDom.style.display = 'none';
    compDom.style.display = 'block';
    projects.style.display = 'none';
    contact.style.display = 'none';
}

navCompDom.addEventListener("click", function () {
    compShow();
    hideNav();
});

function projectsShow() {
    aboutMeDom.style.display = 'none';
    compDom.style.display = 'none';
    projects.style.display = 'block';
    contact.style.display = 'none';
}

navProjectsDom.addEventListener("click", function () {
    projectsShow();
    hideNav();
});

function contactShow() {
    aboutMeDom.style.display = 'none';
    compDom.style.display = 'none';
    projects.style.display = 'none';
    contact.style.display = 'block';
}

navContactDom.addEventListener("click", function () {
    contactShow();
    hideNav();
});

// nav bar show/hide responsive
let iconDom = document.getElementById('icon'),
    ulDom = document.querySelector('ul');

iconDom.addEventListener('click', function () {
    hideNav();
});
